from django import forms
from .models import Status
##Create your forms here

class StatusForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : "buat nama",
        'type' : 'text',
        'maxlength' : '30',
        'required' : True,
        'label' : '',
    }))

    status = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : "buat status",
        'type' : 'text',
        'maxlength' : '100',
        'required' : True,
        'label' : '',
    }))

    
