from .models import Status
from .forms import StatusForm
from django.shortcuts import render,redirect


# Create your views here.

def confirmation(request):
	if (request.method == 'POST'):
		form = StatusForm(request.POST)
		if (form.is_valid()):
			name = form.cleaned_data['name']
			status = form.cleaned_data['status']
			newstatus = Status.objects.create(name=name, status = status)
			newstatus.save()
			return redirect('tugas:status')
	else:
		form = StatusForm()
	return render(request,'pages/confirmation.html')

def status(request):
	stats = Status.objects.all()
	if (request.method == 'POST'):
		form = StatusForm(request.POST)
		if (form.is_valid()):
			username= form.cleaned_data['name']
			userstatus= form.cleaned_data['status']
			response = {'name':username, 'status':userstatus}
			return render(request, 'pages/confirmation.html',response)
	else:
		form = StatusForm()
	return render(request, 'pages/index.html', {'stats': stats, 'form':form})
