from django.db import models

# Create your models here

class Status(models.Model):
    name = models.TextField('name', max_length=30)
    status = models.TextField('status', max_length = 100)

    def __str__(self):
        return self.status
